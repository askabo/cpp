#include <iostream>
#include "mutantstack.hpp"
#include <list>

int main()
{
	MutantStack<int> mstack;
	mstack.push(1);
	mstack.push(2);
	std::cout << "top: " << mstack.top() << std::endl;
	mstack.pop();
	std::cout << "size: "<< mstack.size() << std::endl;
	mstack.push(3);
	mstack.push(4);
	mstack.push(5);
	mstack.push(6);
	std::cout << "top: " << mstack.top() << std::endl;
	std::cout << "size: "<< mstack.size() << std::endl;
	MutantStack<int>::iterator it = mstack.begin();
	MutantStack<int>::iterator ite = mstack.end();
	++it;
	--it;
	while (it != ite)
	{
		std::cout << "*it: " << *it << std::endl;
		++it;
	}
	std::stack<int> s(mstack);
	std::cout << "s.top(): " << s.top() << std::endl << std::endl;
	std::list<int> list;
	list.push_front(1);
	list.push_front(2);
	std::cout << "begin: " << *(list.begin()) << std::endl;
	list.pop_front();
	std::cout << "size: "<< list.size() << std::endl;
	list.push_front(3);
	list.push_front(4);
	list.push_front(5);
	list.push_front(6);
	std::cout << "begin: " << *(list.begin()) << std::endl;
	std::cout << "size: "<< list.size() << std::endl;
	std::list<int>::reverse_iterator itl = list.rbegin();
	std::list<int>::reverse_iterator itle = list.rend();
	++itl;
	--itl;
	while (itl != itle)
	{
		std::cout << "*itl: " << *itl << std::endl;
		++itl;
	}
	return 0;
}
