//
// Created by Daikon Cordelia on 6/16/21.
//

#ifndef MUT_MUTANTSTACK_HPP
#define MUT_MUTANTSTACK_HPP
#include <iostream>
#include <stack>

template <typename T>
class MutantStack: public std::stack<T>
{
public:
	MutantStack() {}

	MutantStack &operator=(const MutantStack<T> &q) { *this = q; return *this; }

	MutantStack(const MutantStack<T> &q) { *this = q; }

	virtual ~MutantStack(){};

	typedef typename std::stack<T>::container_type::iterator iterator;
	iterator	begin() {return (std::stack<T>::c.begin());}
	iterator	end() {return (std::stack<T>::c.end());}
};


#endif //MUT_MUTANTSTACK_HPP
