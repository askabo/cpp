//
// Created by Daikon Cordelia on 6/15/21.
//

#ifndef EASYFIND_EASYFIND_HPP
#define EASYFIND_EASYFIND_HPP
#include <iostream>
#include <list>
#include <algorithm>

template<typename T>
typename T::iterator easyfind(T & container, int i)
{
	typename T::iterator it = find(container.begin(), container.end(), i);
	if (*it != i)
		throw std::exception();
	return it;
}
#endif //EASYFIND_EASYFIND_HPP
