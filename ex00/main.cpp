#include <iostream>
#include <cstdlib>
#include <vector>
#include <deque>
#include <set>
#include "easyfind.hpp"

template <typename T>
void test(T & constructor, int i)
{
	try
	{
		typename T::iterator it = easyfind(constructor, i);
		std::cout << *it << std::endl;
	}
	catch (std::exception & e)
	{
		std::cout << "\033[0;31mError of find\033[0m\n";
	}
}

int main()
{
	srand(time(NULL));
	std::cout << "-----------------List-----------------\n";
	std::list<int> c_list;
	c_list.push_back(5);
	c_list.push_back(2);
	c_list.push_back(3);
	test(c_list, 4);
	test(c_list, 5);
	std::cout << "----------------Vector----------------\n";
	std::vector<int> c_vector;
	c_vector.push_back(15);
	c_vector.push_back(12);
	c_vector.push_back(13);
	test(c_vector, 12);
	test(c_vector, 5);
	std::cout << "----------------Deque----------------\n";
	std::deque<int> c_deque;
	c_deque.push_back(25);
	c_deque.push_back(22);
	c_deque.push_back(23);
	test(c_deque, 23);
	test(c_deque, 15);
	std::cout << "-----------------Set-----------------\n";
	std::set<int> c_set;
	c_set.insert(35);
	c_set.insert(32);
	c_set.insert(33);
	test(c_set, 34);
	test(c_set, 35);
	std::cout << "--------------Good luck--------------\n";
	return 0;
}
