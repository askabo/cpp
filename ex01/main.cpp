#include <iostream>
#include "span.hpp"

int main()
{
	srand(time(NULL));
	Span sp(5);
	std::cout << "---------------Test 0---------------\n";
	try
	{
		std::cout << sp.shortestSpan() << std::endl;
		std::cout << sp.longestSpan() << std::endl;
	}
	catch (std::exception & e)
	{
		std::cout << e.what();
	}
	std::cout << "---------------Test 1---------------\n";
	sp.addNumber(5);
	sp.addNumber(3);
	sp.addNumber(17);
	sp.addNumber(9);
	sp.addNumber(11);
	std::cout << sp.shortestSpan() << std::endl;
	std::cout << sp.longestSpan() << std::endl;
	std::cout << "---------------Test 2---------------\n";
	Span sp_long(100000);
	sp_long.autoAdding(10000);
	std::cout << sp_long.shortestSpan() << std::endl;
	std::cout << sp_long.longestSpan() << std::endl;
	std::cout << "---------------Test 3---------------\n";
	int arrint[] = {0, 10, 20, 30, 40, 50};
	std::multiset<int> copy(arrint, arrint + 6);
	std::multiset<int>::iterator begin = copy.begin();
	std::multiset<int>::iterator end = copy.end();
	try
	{
		sp_long.addNumber(begin, end);
		std::cout << sp_long.shortestSpan() << std::endl;
		std::cout << sp_long.longestSpan() << std::endl;
	}
	catch (std::exception & e)
	{
		std::cout << e.what();
	}
	return 0;
}
