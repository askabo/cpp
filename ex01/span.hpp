//
// Created by Daikon Cordelia on 6/15/21.
//

#ifndef SPAN_SPAN_HPP
#define SPAN_SPAN_HPP
#include <iostream>
#include <cstdlib>
#include <set>

class Span
{
private:
	unsigned int N;
	std::multiset<int> arr;
public:
	class SpanEx : public std::exception
	{
	private:
		char * _error;
	public:
		SpanEx(char * str) : _error(str) {}
		const char *what() const throw() {return _error;}
	};
	Span(unsigned int n);
	Span();
	Span & operator=(Span & src);
	Span(Span & src);
	virtual ~Span();

	void addNumber(int num);
	int shortestSpan();
	int longestSpan();
	void autoAdding(int range);

	template<class Iterator>
	void addNumber(Iterator &begin, Iterator &end)
	{
		unsigned int l = 0;
		Iterator tmp = begin;
		while(tmp != end)
		{
			l++;
			tmp++;
		}
		if (l + this->arr.size() >= N)
			throw SpanEx(const_cast<char *>("\033[0;31mError: not enough space\033[0m\n"));
		std::copy(begin, end, std::inserter(this->arr, this->arr.end()));
	}
};


#endif //SPAN_SPAN_HPP
