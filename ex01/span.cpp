//
// Created by Daikon Cordelia on 6/15/21.
//

#include "span.hpp"

Span::Span(unsigned int n) : N(n)
{}

Span::Span(): N(5)
{}

Span::~Span()
{

}

Span::Span(Span & src)
{
	*this = src;
}

Span & Span::operator=(Span &src)
{
	N = src.N;
	arr = src.arr;
	return *this;
}

void Span::addNumber(int num)
{
	try
	{
		if (arr.size() >= N)
			throw SpanEx(const_cast<char *>("\033[0;31mError: not enough space\033[0m\n"));
		arr.insert(num);
	}
	catch (std::exception & e)
	{
		std::cerr << e.what();
	}
}

int Span::shortestSpan()
{
	if (arr.size() < 2)
		throw SpanEx(const_cast<char *>("\033[0;31mError: not enough numbers\033[0m\n"));
	std::multiset<int>::iterator it = arr.end();
	it--;
	int min = *it;
	it = arr.begin();
	std::multiset<int>::iterator it2 = arr.begin();
	while (++it != arr.end())
	{
		if (min > abs(*it2 - *it))
			min = abs(*it2 - *it);
		if (min == 0)
			break;
		it2++;
	}
	return min;
}

int Span::longestSpan()
{
	if (arr.size() < 2)
		throw SpanEx(const_cast<char *>("\033[0;31mError: not enough numbers\033[0m\n"));
	std::multiset<int>::iterator it1 = arr.begin();
	std::multiset<int>::iterator it2 = arr.end();
	it2--;
	return (*it2 - *it1);
}

void Span::autoAdding(int range)
{
	try
	{
		if (arr.size() >= N)
			throw SpanEx(const_cast<char *>("\033[0;31mError: not enough space\033[0m\n"));
		if (range > (int)N - (int)arr.size())
			throw SpanEx(const_cast<char *>("\033[0;31mError: not enough space\033[0m\n"));
		for (int i = 0; i < range; i++)
			arr.insert(rand());
	}
	catch (std::exception & e)
	{
		std::cerr << e.what();
	}
}